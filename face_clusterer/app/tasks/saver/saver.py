import os
import cv2
import time
import uuid
import redis
import pickle
import logging
from multiprocessing import Pool


def create_folder(path, dir_name):
    full_path = os.path.join(path, dir_name)

    try:
        os.mkdir(full_path)
    except OSError as err:
        full_path = os.path.join(path, dir_name + str(time.time() * 1000))
        os.mkdir(full_path)

    return full_path


class Saver:
    def __init__(self, _processes):
        self.__pool = Pool(_processes)
        self.__r = redis.Redis(host='redis_fc', port=6379)
        self.__path = '/resources/faces'
        logging.info('Saver initialized')

    def shutdown(self):
        logging.info('Saver shutting down')
        self.__pool.close()
        self.__pool.join()

    def __extract_data(self):
        data = dict()

        for key in self.__r.scan_iter():
            data[key.decode('utf_8')] = pickle.loads(self.__r.get(key))

        return data

    def save_faces(self):
        logging.info('Saving found faces...')
        data = self.__extract_data()
        full_path = create_folder(self.__path, 'result')
        jobs = []

        for label in data.keys():
            jobs.append(self.__pool.apply_async(save_certain_face, args=(full_path, label, data[label],)))

        for j in jobs:
            j.get()

        self.__r.flushall()


def save_certain_face(path, label, data):
    logging.info(f'Saving face {label} ...')
    full_path = create_folder(path, 'person' + label)
    for d in data:
        source_photo = d['path']
        (top, right, bot, left) = d['loc']

        ext = source_photo[len(source_photo) - source_photo[::-1].find('.') - 1:]
        _uuid = str(uuid.uuid4()) + ext

        img = cv2.imread(source_photo)
        face = img[top:bot, left:right]

        file_name = os.path.join(full_path, _uuid)

        cv2.imwrite(file_name, face)
