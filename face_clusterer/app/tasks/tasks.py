import sys

sys.path.insert(1, '/app/tasks')

from crawler.crawler import Crawler
from clusterizer.clusterizer import Clusterizer
from saver.saver import Saver


PROCESSES = 6


def task_crawler():
    cr = Crawler(PROCESSES)
    cr.find_handle_photos()
    cr.shutdown()


def task_clusterizer():
    cl = Clusterizer(PROCESSES)
    cl.cluster()
    cl.shutdown()


def task_saver():
    sv = Saver(PROCESSES)
    sv.save_faces()
    sv.shutdown()
