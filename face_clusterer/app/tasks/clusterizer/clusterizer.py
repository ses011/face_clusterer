import pickle
import redis
import logging
import numpy as np
from multiprocessing import Pool
from sklearn.cluster import DBSCAN


class Clusterizer:
    def __init__(self, _processes):
        self.__processes = _processes
        self.__pool = Pool(_processes)
        self.__r = redis.Redis(host='redis_fc', port=6379)
        logging.info('Clusterizer initialized')

    def __extract_data(self):
        logging.info('Extracting data from redis...')
        data = []

        for key in self.__r.scan_iter():
            data.append(pickle.loads(self.__r.get(key)))

        return np.array(data)

    def __save_data(self, clt, data):
        self.__r.flushall()

        logging.info('Redis cleared, saving data...')

        labels = np.unique(clt.labels_)

        for label in labels:
            idxs = np.where(clt.labels_ == label)[0]

            faces = []
            for i in idxs:
                path = data[i]["path"]
                loc = data[i]["loc"]

                faces.append({'path': path, 'loc': loc})

            self.__r.set(str(label), pickle.dumps(faces))

    def cluster(self):
        data = self.__extract_data()
        encodings = [d['encoding'] for d in data]

        logging.info('Clusterizing...')
        clt = DBSCAN(metric='euclidean', n_jobs=self.__processes, min_samples=1, eps=0.4)
        clt.fit(encodings)

        self.__save_data(clt, data)

    def shutdown(self):
        logging.info('Clusterizer shutting down...')
        self.__pool.close()
        self.__pool.join()
