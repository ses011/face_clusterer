import cv2
import uuid
import redis
import pickle
import os.path
import logging
import face_recognition
from os import listdir
from multiprocessing import Pool


ALLOWED_FORMATS = {'.png', '.jpg', '.jpeg'}


r = redis.Redis(host='redis_fc', port=6379)


class Crawler:
    def __init__(self, _processes):
        self.__pool = Pool(_processes)
        self.__path = '/resources/faces'
        r.flushall()
        logging.info('Crawler initialized, Redis cleared')

    def shutdown(self):
        logging.info('Crawler shutting down...')
        self.__pool.close()
        self.__pool.join()

    @staticmethod
    def __correct_format(file):
        last_dot_ind = len(file) - file[::-1].find('.') - 1

        if last_dot_ind == len(file) or file[last_dot_ind:] not in ALLOWED_FORMATS:
            return False
        return True

    def __handle_dir(self, dirs, jobs, d):
        logging.info(f'Handling dir: {d} ...')
        for file in listdir(d):
            full_path = d + '/' + file

            if os.path.isfile(full_path):

                if not self.__correct_format(file):
                    logging.warning(f'File {file} has unknown format')
                else:
                    jobs.append(self.__pool.apply_async(find_faces, args=(full_path,)))
            elif file != 'result':
                dirs.append(file)

    def find_handle_photos(self):
        dirs = [self.__path]
        jobs = []

        while len(dirs) > 0:
            d = dirs.pop()
            self.__handle_dir(dirs, jobs, d)

        logging.info('Waiting for all jobs to finish...')
        for j in jobs:
            j.get()


def find_faces(path):
    logging.info('Finding faces on ' + path)
    image = cv2.imread(path)
    rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    boxes = face_recognition.face_locations(rgb, model='cnn')  # hog
    encodings = face_recognition.face_encodings(rgb, boxes)
    save_data(path, boxes, encodings)


def save_data(path, boxes, encodings):
    for box, enc in zip(boxes, encodings):
        _uuid = uuid.uuid4()

        data = {
            'path': path,
            'loc': box,
            'encoding': enc
        }

        p_data = pickle.dumps(data)

        r.set(str(_uuid), p_data)
