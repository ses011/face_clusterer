from airflow import DAG
from datetime import datetime
from datetime import timedelta
from airflow.operators.python_operator import PythonOperator

from tasks import tasks


with DAG(
        dag_id="cluster",
        schedule_interval="@once",
        default_args={
            "owner": "airflow",
            "retries": 1,
            "retry_delay": timedelta(minutes=5),
            "start_date": datetime(2021, 1, 1),
        },
        catchup=False) as f:

    t_crawler = PythonOperator(
        task_id="find_faces",
        python_callable=tasks.task_crawler
    )

    t_cluster = PythonOperator(
        task_id="split_faces_on_clusters",
        python_callable=tasks.task_clusterizer
    )

    t_save_faces = PythonOperator(
        task_id="save_faces",
        python_callable=tasks.task_saver
    )

t_crawler >> t_cluster >> t_save_faces
